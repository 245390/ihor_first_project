# написать функцию калькулятор, которая принимает на вход строку, содержащую
# два вещественных числа и  один знак арифметической операции и возвращает
# результат выполнения этой операции. Если числа не вещественные или нет знака
# операции, то вызвать исключение ValueError

def calculator(expression):
    allowed = '+-*/'
    if not any(sign in expression for sign in allowed):
        raise ValueError(f'Выражение должно содержать хотя бы один знак({allowed})')
    for sign in allowed:
        if sign in expression:
            try:
                left, right = expression.split(sign)
                left, right = float(left), float(right)
                return {
                    '+': lambda a, b: a + b,
                    '-': lambda a, b: a - b,
                    '*': lambda a, b: a * b,
                    '/': lambda a, b: a / b,
                }[sign](left, right)
            except (ValueError, TypeError):
                raise ValueError('Выражение должно содержать два вещественых числа и один знак')


if __name__ == '__main__':
    string = input('Enter two real numbers and the sign of the operation, no spaces: ')
    print(calculator(string))
