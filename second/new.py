def f(x, y):
  if x == 1 or y == 4:
      return 1
  else:
      return (x + y) * f(x - 1, y + 1)

res = f(3, 2)
print(res)

for i in range(10):
    print()
    for j in range(10):
        print ('*', end="")

flag = False
res = flag and 3 or 4
print(res)